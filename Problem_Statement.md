# Problem Statement

### Grupo: UcSupport

### Membros do Grupo:

```
● Gonçalo Lopes Nº: 2019218223 Email: uc2019218223@student.uc.pt
● Francisco Pires Nº: 2019201225 Email: uc2019201225@student.uc.pt
● Eva Teixeira Nº: 2019215185 Email: uc2019215185@student.uc.pt
● Lucas Gonçalves Nº: 2018287336 Email: uc2018287336@student.uc.pt
```
### Empresa: OldCare

## Introduction

No âmbito da cadeira de Engenharia de Requisitos, criamos um grupo chamado “UcSupport” que tem como objetivo ajudar a empresa OldCare a informatizar os seus serviços de forma a monitorizar de forma constante os seus clientes providenciando-lhes
assim um melhor serviço.
A OldCare é uma empresa de apoio domiciliário a idosos que presta cuidados ao domicílio. Esta empresa apresenta um vasto leque de opções de cuidados prestados que podem ir desde uma simples visita de rotina até cuidados hospitalares como fazer curativos, medir a tensão ou diabetes, entre outros.

## 1. Which problem needs to be solved?

A empresa OldCare trabalha com fichas Clínicas em papel, o que leva a que cada cuidador tenha que transportar pelo menos uma ficha por cada paciente que visita num determinado dia. Isto torna-se bastante ineficiente pois uma simples folha de papel é facilmente trocada ou perdida e consequentemente todos os dados clínicos do paciente em causa.


## 2. Where is the problem?

Uma vez que as fichas vão sendo passadas de mão em mão, de cuidador em cuidador, a empresa não obtém as informações do estado do utente em tempo real. Para além disso, não garante que o historial clínico do cliente seja preservado ao longo do tempo tornando-se assim difícil de verificar alterações de saúde.


## 3. Whose problem is it?

No geral, este é um problema da gerência da empresa mas afeta os empregados diretamente uma vez que estes é que arcam com as consequências caso percam uma ficha de um cliente.


## 4.Why does it need solving?

A empresa está insatisfeita com os métodos tradicionais que possui para guardar os seus dados. O elevado número de dados arquivados dificulta a procura de dados antigos e consequentemente uma tomada de decisão sobre um cliente.