# **PROBLEM STATEMENT & ESTADO DE ARTE**
<br />

**ENGENHARIA DE REQUISITOS**

MESTRADO ENGENHARIA INFORMATICA
OUTUBRO 2022/2023
<br />

#### **Grupo:** UcSupport
#### **Membros do Grupo:**

- Gonçalo Lopes No: 2019218223 Email: uc2019218223@student.uc.pt
- Francisco Pires No: 2019201225 Email: uc2019201225@student.uc.pt
- Eva Teixeira No: 2019215185 Email: uc2019215185@student.uc.pt
- Lucas Gonçalves No: 2018287336 Email: uc2018287336@student.uc.pt

#### **Empresa:** OldCare

<br /><br /><br />

# Problem Statement


### Introduction

No âmbito da cadeira de Engenharia de Requisitos, criamos um grupo chamado “UcSupport” que tem como objetivo ajudar a empresa OldCare a informatizar os seus serviços de forma a monitorizar de forma constante os seus clientes providenciando-lhes assim um melhor serviço.
A OldCare é uma empresa de apoio domiciliário a idosos que presta cuidados ao domicílio. Esta empresa apresenta um vasto leque de opções de cuidados prestados que podem ir desde uma simples visita de rotina até cuidados hospitalares como fazer curativos, medir a tensão ou diabetes, entre outros.

### 1. Which problem needs to be solved?

A empresa OldCare trabalha com fichas Clínicas em papel, o que leva a que cada cuidador tenha que transportar pelo menos uma ficha por cada paciente que visita num determinado dia. Isto torna-se bastante ineficiente pois uma simples folha de papel é facilmente trocada ou perdida e consequentemente todos os dados clínicos do paciente em causa.


### 2. Where is the problem?

Uma vez que as fichas vão sendo passadas de mão em mão, de cuidador em cuidador, a empresa não obtém as informações do estado do utente em tempo real. Para além disso, não garante que o historial clínico do cliente seja preservado ao longo do tempo tornando-se assim difícil de verificar alterações de saúde.


### 3. Whose problem is it?

No geral, este é um problema da gerência da empresa mas afeta os empregados diretamente uma vez que estes é que arcam com as consequências caso percam uma ficha de um cliente.


### 4.Why does it need solving?

A empresa está insatisfeita com os métodos tradicionais que possui para guardar os seus dados. O elevado número de dados arquivados dificulta a procura de dados antigos e consequentemente uma tomada de decisão sobre um cliente.


### 5.How might a software system help?

A empresa quer ter os dados em tempo real para garantir que não há oscilações nos dados clínicos do cliente e que caso haja a gerência ser devidamente notificada assim como um instituto de saúde ou um profissional médico. Para além disso, a informatização desta empresa vai garantir que os dados não são perdidos.


### 6.When does it need solving?

O nosso grupo de trabalho compromete-se a fornecer uma solução até ao final de Fevereiro de 2023.


### 7. What might prevent us from solving it?

A aplicação pode ser complexa demais para algumas pessoas. O facto das cuidadoras serem muito tradicionais e não estarem habituadas a utilizar tablets/computadores ou até o facto destas estarem satisfeitas a tirar os dados à mão, podem ser um entrave à aceitação deste software. Alguns idosos podem viver em aldeias remotas sem acesso a rede, o que continuaria a impedir que os dados fossem carregados em tempo real.
<br /><br /><br />
# **Estado de arte**
 ## Introdução


Foi efetuada uma pesquisa acerca de softwares semelhantes ao que pretendemos implementar na empresa OldCare, um estudo de mercado, para percebermos quantas aplicações semelhantes a estas existem e o que é que elas oferecem.
Concluímos que existem imensas aplicações deste mesmo género e com o mesmo intuito, e algumas delas possuem uma vasta gama de opções e recursos.

### Critérios de procura e palavras-chave

- Softwares para gestão de IPSS
- United Kingdom Homecare Software
- Domiciliary Care Software
- Healthcare Software Development

### Features a incluir no nosso projeto

Face ao enorme volume de softwares existente para solucionar o problema da nossa empresa, consideramos importante possuir uma imagem que nos caracterize e destaque, e isso passa por possuir funcionalidades úteis e, acima de tudo, que sejam frequentemente utilizadas. Deste modo, tentamos selecionar algumas das funcionalidades mais usadas e as que consideramos serem úteis ao nosso cliente. Desta forma, vamos possuir um software intuitivo, eficaz e simples de usar. Ao analisarmos as funcionalidades oferecidas por alguns dos softwares observados, encontramos algumas features bastante interessantes que destacamos e inserimos numa tabela para efetuar uma comparação entre esses recursos e os seus respectivos softwares. Com a ajuda da tabela seguinte, conseguimos perceber quais as funcionalidades que diferenciam e destacam cada um dos softwares listados.


### Tabela de comparação de softwares

#### **My senior**  (Portugal) 
Usado em: Estruturas Residenciais, Casas de Repouso, Centros de Dia e Apoio Domiciliário
```
Diários clínicos;
Registos diários;
Fichas dos utentes (dados, documentos, diários);
Registo de atividades (lanchar, vestir, banho);
Calendarização das atividades;
Painel de ocorrências (ex.: ferida, agressão, dor);
Painel de alertas (ex.: manter o portão fechado);
Registo dos medicamentos e dosagens do utente;
Documento de acompanhamento (ficha com os dados do utente para episódios de consulta médica ou emergência).
```

#### **officegest**  (Portugal) 
Usado em : IPSS 
```
Lista de utentes;
Relatórios de gestão e com todos os dados referentes à gestão da IPSS;
Portal familiar para verificar atividades e ocorrências;
Gestão de tratamentos dos utentes: consultas, exames, estados, avisos.
```

#### **f3m**  (Portugal)
Usado em: IPSS /Misericórdias
```
Geração de faturas e recibos;
Controlo de faltas com possibilidade de dedução na mensalidade;
Controlo dos Utentes em lista de espera;
Cálculo Automático da Mensalidade;
Registo de procedimentos realizados pela enfermagem;
Registo de sinais vitais, de glicémia capilar, de resultados de análises (configurável);
Ficha de Funcionário;
Cálculo Automático de Escalas de Serviço;
Alocação Automática de funcionários a
Escalas de Serviço onde se detetam falhas em termos de número mínimo de funcionários por turno;
Emissão de Escalas de Serviço;
Gestão de Turnos.
Geração de marcações por dia de semana;
Ficha de Viatura de domicílio;
Manutenção de Requisições e Marcações de Viaturas por Data/Hora;
Gestão dos Serviços efetuados por Viatura referenciando os Períodos,
Quilómetros, Tempo Gasto, etc.;
Controlo rigoroso e global das despesas com as viaturas e respetivos abastecimentos;
Controlo de multas/acidentes com possibilidade de anexar documentos;
Vários mapas e gráficos de análise: consumos e médias por viatura, despesas por serviço efetuado ou valência, distribuição mensal das despesas, etc.
```

#### **Ankira**  (Portugal)
Usado em: IPSS 
```
Gestão de Dashboard com eventos, consultas e exames médicos, aniversários, atividades, ocorrências e inscrições;
Registo de pedidos de inscrição;
Plano individual para cada cliente;
Área de monitorização das despesas e cuidados prestados aos utentes em tempo real;
Consultar e exportar planilha de serviço com a distribuição das tarefas;
Estatísticas e indicadores, com filtros.
```

#### **carebeans**  (UK)
Usado em: Apoio Domiciliário
```
Dashboard com atividades e alertas diárias;
Calendário com agendamento de visitas e atividades (calendário de arrastar e soltar tarefas);
Alterações de agendamento de um clique, “arrastar e soltar” enviadas automaticamente para aplicativos móveis da equipe de atendimento;
Portal de família - Permitir que a família dos usuários do serviço veja o que eles querem ver em relação aos cuidados prestados aos seus entes queridos.
```

#### **CarePlanner** (UK)
Usado em: Apoio Domiciliário
```
Checklist lateral - to do;
Dashboard com horários que os utentes necessitam de atenção e carregando no nome deles consegue-se selecionar uma cuidadora disponível.
```

#### **BirdieCare** (UK)
Usado em: Apoio Domiciliário
```
Alerta de medicação;
Quadro para planear e agendar tarefas;
Quadro para agendamento de visitas;
Mapa corporal - selecionar a parte do corpo e local exato e adicionar uma nota que emitirá um aviso (ex.: ferida no dedo).
Seleciona também o tipo de preocupação (ferida, medicação, acidente).
App cuidador com “ checklist to do” de tarefas associado ao nome de cada utente.
```

#### **alorahealth** (USA)
Usado em:  Apoio domiciliário
```
Calendarização de atividades;
Calcular automaticamente o tempo de viagem e a quilometragem, utilizando um serviço de mapeamento, para a viagem do seu cuidador de uma visita de paciente para outra;
Os cuidadores podem aceder às suas agendas, encontrar direções para a casa do paciente, documentar registos de comunicação e muito mais;
Assistência médica, com médico disponível no software;
Alerta para as datas de validade dos documentos dos cuidadores (carta de condução, seguro do carro).
```


#### **smartcaresoftware** (USA)
Usado em: Apoio domiciliário
```
Faturamento automático;
Dividir faturas entre vários pagadores;
Portal de pagamento on-line do cliente;
Armazena com segurança os pagadores e métodos de pagamento para processamento automático;
Recursos de telefonia;
Recomendações inteligentes do cuidador e correspondência;
Alerta de horas extras;
Alertas em tempo real, turnos atrasados e perdidos;
Exibições flexíveis de agendamento de cliente/equipe com um clique.
```

#### **access** (UK)
Usado em: Apoio domiciliário
```
Escrever notas com recurso ao microfone;
Guardar fotos nas observações;
Cursos iterativos online;
Avaliação de competências virtual;
Envia avisos e instruções sobre quais medicamentos administrar durante cada turno ou visita;
Registar quando a medicação foi administrada;
Dar aos profissionais de cuidados acesso a detalhes atuais no ponto de prestação de cuidados;
Receber alertas quando a medicação ainda não foi tomada;
```
<br /><br />
Após esta análise, encontramos algumas features úteis, que encaixam perfeitamente no
nosso projeto e que vão de acordo com aquilo que o cliente procura. Estas são as seguintes:

- Assistência médica
- Botão de alerta
- Distribuir cuidadores de acordo com a localização do utente
- Painel de ocorrências e alertas
- Gestão dos Serviços efetuados por Viatura referenciando os Períodos, Quilómetros, Tempo Gasto
- Cada cuidador possui uma “ _checklist to d_ o” com as suas tarefas
- Alertas em tempo real, turnos atrasados e perdidos
- Alertas para tomar a medicação


### Referências

Para responder ao solicitado, visitamos os seguintes sites:

- **https://ankira.pt/?utm_source=google&utm_medium=cpc&utm_campaign&ut**
    **m_adgroup&gclid=Cj0KCQjw-**
    **fmZBhDtARIsAH6H8qjKjEspCvHxlAS8VFO3V5foVNVzWPvuWVrL1ttq_3DMvu**
    **yw15TuRpkaAs1CEALw_wcB**
- **https://www.carebeans.co.uk/domiciliary-care-**
    **software/ https://www.f3m.pt/pt/software/ipss/gestao-de-ipss-misericordias**
- **https://pt.officegest.com/gestao-de-ipss/**
- **https://mysenior.com/?gclid=CjwKCAjws--ZBhAXEiwAv-**
    **RNLwrjrCBrNkwQAsgQoMuQk1l4_-**
    **BzeDAsajh3VUrBKmAkqi8yTWVHLRoCFYAQAvD_BwE**
- **https://www.care-planner.co.uk/careplanner-product-tour/**
- **https://www.birdie.care/product-features**
- **https://www.alorahealth.com/home-care-software/**
- **https://smartcaresoftware.com/solutions/back-office/**
- **https://www.theaccessgroup.com/en-gb/health-social-care/sectors/home-**
    **care/**


